﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UImassObject : MonoBehaviour {

    public GameObject capsule;
    public InputField mainInputField;

    // Use this for initialization
    void Start() {
        mainInputField.text = capsule.GetComponent<Rigidbody>().mass.ToString("#");
    }

    // Update is called once per frame
    void Update() {

    }

    public void ChangeMassObject() {
        if (mainInputField.text != "") {
            capsule.GetComponent<Rigidbody>().mass = float.Parse(mainInputField.text);
        }
    }
}
