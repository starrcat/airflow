﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionDetector : MonoBehaviour {

    private Material m_Material;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "ExperimentalBox")
        {

            m_Material = collision.gameObject.GetComponent<Renderer>().material;
            m_Material.color = Color.red;
            //Debug.Log(collision.gameObject.GetComponent<Renderer>().material);
            //Debug.Log(Resources.FindObjectsOfTypeAll(typeof(Material))[0]);

        }
    }

}
