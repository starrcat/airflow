﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class DebugTrajectory : MonoBehaviour {
    public bool activate = true;
    public float startLineWidth = 0.5f;
    public float endLineWidth = 0.5f;
    public Material material;

    protected LineRenderer line;
    protected Vector3 position;

    public static UnityEvent ToggleActiveEvent;

    public Toggle toggle;

    void Start () {

        if (ToggleActiveEvent == null)
            ToggleActiveEvent = new UnityEvent();

        ToggleActiveEvent.AddListener(ToggleActive);

        if (activate) {
            OnActivate();
        }
    }

    void Update() {
        if (activate) {
            position = gameObject.transform.position;
            line.positionCount++;
            line.SetPosition(line.positionCount - 1, position);
        }
    }

    void OnActivate()
    {
        line = gameObject.AddComponent<LineRenderer>();
        line.startWidth = endLineWidth;
        line.endWidth = startLineWidth;
        line.material = material;
        position = gameObject.transform.position;
        line.SetPosition(0, position);
        line.positionCount--;
    }

    void ToggleActive()
    {
        activate = toggle.isOn;
        if (activate)
        {
            OnActivate();
        } else
        {
            Destroy(line);
        }
    }
    
}
