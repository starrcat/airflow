﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamFly : MonoBehaviour {

    // Use this for initialization
    public float cameraSensitivity = 90;
    public float climbSpeed = 4;
    public float normalMoveSpeed = 10;
    public float slowMoveFactor = 0.25f;
    public float fastMoveFactor = 3;

    private float rotationX = 0.0f;
    private float rotationY = 0.0f;

    public GameObject FlightTab;
    private CameraFollower cf;

    private const float FIXED_TIME = .02f; // or Time.deltaTime

    private bool isGhost = false;
    private bool onMove = false;

    void Start()
    {
        CheckCursor();
        cf = gameObject.GetComponent<CameraFollower>();
    }

    void Update()
    {

        if (Input.GetKeyDown(KeyCode.V))
        {
            if (!cf.isPov)
            {
                // Turn on Ghost only if POV mode is disabled.
                isGhost = !isGhost;
            }
        }

        if (Input.GetKey(KeyCode.LeftAlt))
        {
            CameraRotates();
        }

        // Movement
        if (isGhost)
        {

            // Camera rotation
            CameraRotates();

            // Speed Up
            if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
            {
                transform.position += transform.forward * (normalMoveSpeed * fastMoveFactor) * Input.GetAxis("Vertical") * FIXED_TIME;
                transform.position += transform.right * (normalMoveSpeed * fastMoveFactor) * Input.GetAxis("Horizontal") * FIXED_TIME;
            } // Slow down
            else if (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl))
            {
                transform.position += transform.forward * (normalMoveSpeed * slowMoveFactor) * Input.GetAxis("Vertical") * FIXED_TIME;
                transform.position += transform.right * (normalMoveSpeed * slowMoveFactor) * Input.GetAxis("Horizontal") * FIXED_TIME;
            }
            else // Just moves
            {
                transform.position += transform.forward * normalMoveSpeed * Input.GetAxisRaw("Vertical") * FIXED_TIME;
                transform.position += transform.right * normalMoveSpeed * Input.GetAxisRaw("Horizontal") * FIXED_TIME;
            }


            if (Input.GetKey(KeyCode.Q)) { transform.position += transform.up * climbSpeed * FIXED_TIME; }
            if (Input.GetKey(KeyCode.E)) { transform.position -= transform.up * climbSpeed * FIXED_TIME; }
        }
        // Checks cursor visible
        CheckCursor();
    }

    private void CameraRotates()
    {
        // Camera rotation
        rotationX += Input.GetAxis("Mouse X") * cameraSensitivity * FIXED_TIME;
        rotationY += Input.GetAxis("Mouse Y") * cameraSensitivity * FIXED_TIME;
        rotationY = Mathf.Clamp(rotationY, -90, 90);

        transform.localRotation = Quaternion.AngleAxis(rotationX, Vector3.up);
        transform.localRotation *= Quaternion.AngleAxis(rotationY, Vector3.left);
    }

    private void CheckCursor()
    {
        Cursor.visible = !isGhost;
        if (isGhost)
        {
            Cursor.lockState = CursorLockMode.Locked;
        } else
        {
            Cursor.lockState = CursorLockMode.None;
        }
        FlightTab.SetActive(isGhost);
    }

    public void TurnOffGhost()
    {
        isGhost = false;
    }
}
