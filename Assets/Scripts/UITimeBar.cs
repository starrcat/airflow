﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UITimeBar : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnTimeChanged(Slider obj)
    {
        if (obj.value >= 0 && obj.value <= 1)
        {
            Time.timeScale = obj.value;
            Time.fixedDeltaTime = obj.value * .02f;
        }
    }
}
