﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateArrow : MonoBehaviour {

    float rotSpeed = 300.0f;
    public Camera cam;
    public GameObject windControll;
    bool drag = false;

    // Update is called once per frame
    void Update () {
		if (!drag) {
            transform.rotation = Quaternion.Euler(
                windControll.transform.rotation.eulerAngles.x - cam.transform.rotation.eulerAngles.x,
                windControll.transform.rotation.eulerAngles.y - cam.transform.rotation.eulerAngles.y - 90,
                windControll.transform.rotation.eulerAngles.z - cam.transform.rotation.eulerAngles.z
            );
            
        }
	}

    private void OnMouseDrag() {
        drag = true;
        float rotX = Input.GetAxis("Mouse X") * rotSpeed * Mathf.Deg2Rad;
        float rotY = Input.GetAxis("Mouse Y") * rotSpeed * Mathf.Deg2Rad;

        transform.Rotate(Vector3.up, -rotX, Space.World);
        transform.Rotate(Vector3.left, rotY, Space.World);
        //transform.rotation = Quaternion.Euler(0, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z);
        windControll.transform.Rotate(Vector3.up, -rotX, Space.World);
        windControll.transform.Rotate(Vector3.left, rotY, Space.World);
        drag = false;
    }
}
