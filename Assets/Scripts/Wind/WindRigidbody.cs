﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindRigidbody : MonoBehaviour {
    public float windStrength = 4.0f;
    private float radius = 1234567.0f;
    private int i;
    public Transform windTransformPosition;
    public Transform windTransformRotation;

    // Update is called once per frame
    void Update() {
        if (windTransformPosition != null && windTransformRotation != null) {
            windTransformRotation.rotation = transform.rotation;
            var hitColliders = Physics.OverlapSphere(windTransformPosition.transform.position, radius);
            
            for (i = 0; i < hitColliders.Length; i++) {
                if (hitColliders[i].GetComponent<Rigidbody>() != null) {
                    RaycastHit hit;
                    var rayDirection = hitColliders[i].GetComponent<Rigidbody>().gameObject.transform.position - windTransformPosition.transform.position;
                    if (Physics.Raycast(windTransformPosition.transform.position, rayDirection, out hit)) {
                        if (hit.transform.GetComponent<Rigidbody> ()) {
                            if (hitColliders[i].gameObject.tag == "Player") {
                                hitColliders[i].GetComponent<Rigidbody>().AddForce(windTransformPosition.transform.forward * windStrength, ForceMode.Acceleration);
                            }
                        }
                    }

                }
            }
        }
    }
}
