﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;
using UnityEngine.UI;

public class SceneControl : MonoBehaviour {
    private bool pause = false;
    public Text PlayPauseBtnText;
    public Slider TimeScaleSlider;

    private float previousTimeScale = 1.00f;

    public void ReloadScene() {
        //SceneManager.LoadScene("SampleScene");
        //Time.timeScale = 1.00f;
        //TimeBody tb = GetComponent<TimeBody>();
        TimeBody.ResetEvent.Invoke();
    }

    public void PauseAndPlayScene() {
        if (pause) {
            Time.timeScale = previousTimeScale;
            TimeScaleSlider.interactable = true;
            pause = false;
            PlayPauseBtnText.text = "Остановить симуляцию";
        }
        else {
            previousTimeScale = Time.timeScale;
            Time.timeScale = 0.00f;
            TimeScaleSlider.interactable = false;
            pause = true;
            PlayPauseBtnText.text = "Продолжить симуляцию";
        }
    }


}
