﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIsrenghtWind : MonoBehaviour {
    public GameObject wind;
    public InputField mainInputField;

	// Use this for initialization
	void Start () {
        //mainInputField.text = wind.GetComponent<WindArea>().strength.ToString("#");
        mainInputField.text = wind.GetComponent<WindRigidbody>().windStrength.ToString("#");
        
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ChangeStrenghtWind() {
        if (mainInputField.text != "") {
            //wind.GetComponent<WindArea>().strength = float.Parse(mainInputField.text);
            wind.GetComponent<WindRigidbody>().windStrength = float.Parse(mainInputField.text);
        }
    }
}
