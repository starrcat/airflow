﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollower : MonoBehaviour {

    public GameObject cube;
    private Vector3 offset;
    private Vector3 prevCamPos;
    private Quaternion prevCamRot;
    public bool isPov = false;
    private CamFly camFly;

	// Use this for initialization
	void Start () {
        //offset = transform.position - cube.transform.position;
        savePrevPositionAndRotation();
        camFly = gameObject.GetComponent<CamFly>();
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.P))
        {
            if (isPov)
            {
                isPov = false;
                transform.position = prevCamPos;
                transform.rotation = prevCamRot;
            } else // Turn on POV mode.
            {
                // Don't forget to turn off Ghost mode.
                camFly.TurnOffGhost();

                savePrevPositionAndRotation();
                isPov = !isPov;
            }
        }

        if (isPov)
        {
            transform.position = cube.transform.position;
            transform.rotation = new Quaternion(transform.rotation.x, cube.transform.rotation.y, transform.rotation.z, transform.rotation.w);
        }
	}

    private void savePrevPositionAndRotation()
    {
        prevCamPos = transform.position;
        prevCamRot = transform.rotation;
    }
}
