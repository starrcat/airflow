﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TimeBody : MonoBehaviour {

    private bool isRewinding = false;
    public float recordTime = 5f;


    List<PointInTime> pointsInTime;
    Rigidbody rb;

    public static UnityEvent ResetEvent;
    PointInTime inception;

	// Use this for initialization
	void Start () {
        pointsInTime = new List<PointInTime>();
        rb = GetComponent<Rigidbody>();

        inception = new PointInTime(transform.position, rb.rotation, rb.velocity);

        if (ResetEvent == null)
            ResetEvent = new UnityEvent();

        ResetEvent.AddListener(TotalRecall);
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKeyDown(KeyCode.Backspace) && Time.timeScale == 1)
            StartRewind();
        if (Input.GetKeyUp(KeyCode.Backspace))
            StopRewind();

	}

    private void FixedUpdate()
    {
        if (isRewinding)
            Rewind();
        else
            Record();
    }

    void Rewind()
    {
        if (pointsInTime.Count > 0)
        {
            PointInTime pointInTime = pointsInTime[0];

            transform.position = pointInTime.position;
            rb.rotation = pointInTime.rotation;
            rb.velocity = pointInTime.velocity;

            pointsInTime.RemoveAt(0);
        } else
        {
            StopRewind();
        }
    }

    void Record()
    {
        if (pointsInTime.Count > Mathf.Round(recordTime / Time.fixedDeltaTime))
        {
            pointsInTime.RemoveAt(pointsInTime.Count - 1);
        }
        pointsInTime.Insert(0, new PointInTime(transform.position, rb.rotation, rb.velocity));
    }

    public void StartRewind()
    {
        isRewinding = true;
        //rb.isKinematic = true;
    }

    public void StopRewind()
    {
        isRewinding = false;
        //rb.isKinematic = false;
    }

    public void TotalRecall()
    {
        rb.isKinematic = true;
        rb.velocity = new Vector3(0,0,0);
        rb.rotation = inception.rotation;
        transform.position = inception.position;
        rb.isKinematic = false;
    }
}
